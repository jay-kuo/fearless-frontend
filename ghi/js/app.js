function formatDate(dateString) {
    const newDate = new Date(dateString).toLocaleDateString('en-US');
    return newDate;
}


function createCard(name, location, description, pictureUrl, starts, ends) {
    const newStarts = formatDate(starts)
    const newEnds = formatDate(ends)
    return `
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
    <div class="grid gap-0 row-gap-2">
    <div class="grid gap-0 column-gap-3">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
                <p class="card-date">${newStarts} - ${newEnds}</p>
            </div>
        </div>
    </div>
    </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            console.error("An error has occurred:");
        } else {
            const data = await response.json();

            // const conference = data.conferences[0];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) {
            //     const details = await detailResponse.json();
            //     const descriptionTag = document.querySelector('.card-text');
            //     descriptionTag.innerHTML = details.conference.description;
            //     const imageTag = document.querySelector('.card-img-top');
            //     imageTag.src = details.conference.location.picture_url;
            // }

            const columns = document.querySelectorAll('.col');
            let columnIndex = 0;

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const html = createCard(name, location, description, pictureUrl, starts, ends);
                    const column = columns[columnIndex];
                    column.innerHTML += html;
                    columnIndex = (columnIndex + 1) % columns.length;
                }
            }
        }
    } catch (e) {
        console.error("An error occurred", e);
    }
});
